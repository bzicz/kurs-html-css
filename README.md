# HTML i CSS
> Kilka ważnych informacji

Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki

## Jak zacząć?

1. Stwórz [*fork*][forking] repozytorium z zadaniami.
2. [*Sklonuj*][ref-clone] repozytorium na swój komputer.
3. Rozwiąż zadania i [*skomituj*][ref-commit] zmiany do swojego repozytorium.
4. [*Wypchnij*][ref-push] zmiany do swojego repozytorium na GitHubie.
5. Stwórz [*pull request*][pull-request] do oryginalnego repozytorium, gdy skończysz wszystkie zadania.

## Plan tego repozytorium

* Zadania
    * Tutaj znajdziesz zadania do rozwiązania. Kilka na wstępie jest dla wykładowcy, aby pokazał Ci jak rozwiązywać kolejne. Pozostałe zadania wykonaj samodzielnie w domu. Będą one sprawdzane przez mentora. Pamiętaj, aby każde zadanie było opisane.
    * Zadania dodatkowe są dla chętnych i ambitnych. Ty też jesteś, prawda? :)

* SNIPPET.md
    * Tutaj znajdziesz kawałki kodu rozwiązujące różne problemy, mogą Ci się przydać w trakcie rozwiązywania zadań.

### Poszczególne zadania rozwiązuj w odpowiednich plikach.

<!-- Links -->
[forking]: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
[ref-clone]: https://confluence.atlassian.com/bitbucketserver/clone-a-repository-790632786.html#Clonearepository-Cloningamirrorrepository
[ref-commit]: http://gitref.org/basic/#commit
[ref-push]: http://gitref.org/remotes/#push
[pull-request]: https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html
